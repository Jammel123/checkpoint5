const randomstring = require("randomstring");
const puppeteer = require("puppeteer");
let page;
let browser;
//Variables
// random department desc
const remarks = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
// random first name
const fname = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Middle Name
const mname = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Last Name
const lname = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Email
const email = randomstring.generate({
  length: 4,
  charset: "alphabetic"
});
//random Mobile number
const mobile = randomstring.generate({
  length: 11,
  charset: "numeric"
});
//========================= L O G I N  A S   D E P A R T M E N T  H E A D =============================
test("Login as HIRING OFFICER (PG)", async () => {
  browser = await puppeteer.launch({
    headless: true,
    slowMo: 30,
    args: ["--proxy-server= 172.16.1.6:3128", "--start-fullscreen"]
  });
  page = await browser.newPage();

  await page.goto("http://rectrack.jltechsol.com/login");

  await page.setViewport({ width: 1366, height: 768 });

  //Input Username
  await page.click(".elevation-12 #email");
  await page.type(".elevation-12 #email", "dp.jammel@gmail.com");

  //Input Password
  await page.click(".elevation-12 #password");
  await page.type(".elevation-12 #password", "123456789");

  //Login Button

  await page.waitForSelector(".elevation-12 #loginButton");
  await page.click(".elevation-12 #loginButton");

  //dashboard
  await page.waitForSelector(".v-responsive #sidebar1");
  await page.click(".v-responsive #sidebar1");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-responsive #sidebar1").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 2000000);

//========================   R E C E I V E D   A P P L I C A N T   =========================================
//-----------Validation for Department Head can view list of all received applications-----------------
test("Validation for Department Head can view list of all received applications", async () => {
  //received
  await page.waitForSelector(
    ".v-tabs__wrapper > .v-tabs__container > #ReceivedUsers > .v-tabs__item > .v-badge"
  );
  await page.click(
    ".v-tabs__wrapper > .v-tabs__container > #ReceivedUsers > .v-tabs__item > .v-badge"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__wrapper > .v-tabs__container > #ReceivedUsers > .v-tabs__item > .v-badge"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-------Validation for Department Head can search received applicant------
test("Validation for Department Head can search received applicant", async () => {
  let input__slotsrec;
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsrec = await page.$("#search_dashboard");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("gate");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#search_dashboard").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-----Department Head can choose action View Resume------
test("Validation for Department Head can choose action View Resume", async () => {
  // Click View Resume
  await page.waitForSelector(
    "tbody > tr:nth-child(1) > td > #receive_view > .v-btn__content"
  );
  await page.click(
    "tbody > tr:nth-child(1) > td > #receive_view > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr:nth-child(1) > td > #receive_view > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-----Department Head can choose action Online Exam-------
test("Validation for Department Head can choose action Online Exam", async () => {
  let input__slotsrec;
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //received
  await page.waitForSelector(
    ".v-tabs__wrapper > .v-tabs__container > #ReceivedUsers > .v-tabs__item > .v-badge"
  );
  await page.click(
    ".v-tabs__wrapper > .v-tabs__container > #ReceivedUsers > .v-tabs__item > .v-badge"
  );
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsrec = await page.$("#search_dashboard");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("gate");
  await page.keyboard.press("Enter");

  //Click Online  Exam
  await page.waitForSelector(
    "tbody > tr:nth-child(1) > td > #receive_onlineexam > .v-btn__content"
  );
  await page.click(
    "tbody > tr:nth-child(1) > td > #receive_onlineexam > .v-btn__content"
  );

  //Show dialog box
  page.on("dialog", async dialog => {
    await dialog.accept();
  });

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr:nth-child(1) > td > #receive_onlineexam > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-----Department Head can choose action Blacklist------------
test("Validation for Department Head can choose action Blacklist", async () => {
  let input__slotsrec;
  await page.waitFor(2000);
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });
  //sidebar
  await page.waitForSelector(
    ".v-responsive__content > .v-list > .v-list-item:nth-child(3) > #sidebar2 > .v-list__tile__title"
  );
  await page.click(
    ".v-responsive__content > .v-list > .v-list-item:nth-child(3) > #sidebar2 > .v-list__tile__title"
  );
  //received
  await page.waitForSelector(
    ".v-tabs__wrapper > .v-tabs__container > #ReceivedUsers > .v-tabs__item > .v-badge"
  );
  await page.click(
    ".v-tabs__wrapper > .v-tabs__container > #ReceivedUsers > .v-tabs__item > .v-badge"
  );

  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsrec = await page.$("#search_dashboard");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("rodney");
  await page.keyboard.press("Enter");

  // Click Blacklist
  await page.waitForSelector(
    "tbody > tr:nth-child(1) > td > #receive_blacklist > .v-btn__content"
  );
  await page.click(
    "tbody > tr:nth-child(1) > td > #receive_blacklist > .v-btn__content"
  );

  //Remarks
  await page.click(".v-input #remarks");
  await page.type(".v-input #remarks", `${remarks}`);
  //Yes Button
  await page.waitForSelector(
    ".v-card > .v-card__actions > #setBlacklist > #setBlacklist > .v-btn__content"
  );
  await page.click(
    ".v-card > .v-card__actions > #setBlacklist > #setBlacklist > .v-btn__content"
  );
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-card > .v-card__actions > #setBlacklist > #setBlacklist > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//=============================== O N L I N E   E X A M ====================================
//----------Validation for Department Head can view list of all applications for online exam-----
test("Validation for Department Head can view list of all applications for online exam", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Online Exam
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #OnlineExamUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #OnlineExamUsers > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #OnlineExamUsers > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//--------Validation for Department Head can search for online exam applicant -------
test("Validation for Department Head can search for online exam applicant ", async () => {
  let input__slotsrec;
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsrec = await page.$("#search_dashboard");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("vViD");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#search_dashboard").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-------Validation for Department Head wants to view Applicants' Resume-----------
test("Validation for Department Head wants to view Applicants' Resume", async () => {
  // Click View Resume
  await page.waitForSelector("tbody > tr > td > #oe_view > .v-btn__content");
  await page.click("tbody > tr > td > #oe_view > .v-btn__content");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #oe_view > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//--------------Validation for Department Head wants to reject applicants--------------------
test("Validation for  Department Head choose to Reject the Applicant based on the result of online exam", async () => {
  let input__slotsendo;
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Online Exam
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #OnlineExamUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #OnlineExamUsers > .v-tabs__item"
  );

  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsendo = await page.$("#search_dashboard");
  await input__slotsendo.click({ clickCount: 1 });
  await input__slotsendo.type("WidH");
  await page.keyboard.press("Enter");

  //Click Reject
  await page.waitForSelector("tbody > tr > td > #oe_reject > .v-btn__content");
  await page.click("tbody > tr > td > #oe_reject > .v-btn__content");

  //Remarks
  await page.click(".v-input #remarks");
  await page.type(".v-input #remarks", "good");

  //Yes Button in Remarks
  await page.waitForSelector(
    ".v-card > .v-card__actions > #setReject > #setReject > .v-btn__content"
  );
  await page.click(
    ".v-card > .v-card__actions > #setReject > #setReject > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-card > .v-card__actions > #setReject > #setReject > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//============================  E N D O R S E M E N T ===================================
//----------Validation for Department Head can view list of all applications for endorsement-----
test("Validation for Department Head can view list of all applications for endorsement", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });
  await page.waitFor(2000);

  //Endorsement
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #EndorsedUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #EndorsedUsers > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #EndorsedUsers > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------Validation for Department Head can search for endorsement applicant ---------
test("Validation for Department Head can search for endorsement applicant", async () => {
  let input__slotsendo;
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsendo = await page.$("#search_dashboard");
  await input__slotsendo.click({ clickCount: 1 });
  await input__slotsendo.type("yeaux");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#search_dashboard").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-------------Validation for Department Head wants to view Applicants' Resume---------
test("Validation for Department Head wants to view Applicants' Resume", async () => {
  // Click View Resume
  await page.waitForSelector(
    "tbody > tr > td > #enbdorse_view > .v-btn__content"
  );
  await page.click("tbody > tr > td > #enbdorse_view > .v-btn__content");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #enbdorse_view > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//--------Validation for Department Head choose to view exam result in online exam-------
test("Validation for Department Head choose to view exam result in online exam", async () => {
  let input__slotsendo;
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Endorsement
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #EndorsedUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #EndorsedUsers > .v-tabs__item"
  );

  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsendo = await page.$("#search_dashboard");
  await input__slotsendo.click({ clickCount: 1 });
  await input__slotsendo.type("yeaux");
  await page.keyboard.press("Enter");

  //Click Endorse
  await page.waitForSelector(
    "tbody > tr > td > #endorse_endorsed > .v-btn__content"
  );
  await page.click("tbody > tr > td > #endorse_endorsed > .v-btn__content");

  //show dialog
  page.on("dialog", async dialog => {
    await dialog.accept();
  });

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #endorse_endorsed > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----Validation for Department Head choose to endorsed Applicant to PG based on the result of online exam-------
test("Validation for Department Head choose to endorsed Applicant to PG based on the result of online exam", async () => {
  let input__slotsendo;
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Endorsement
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #EndorsedUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #EndorsedUsers > .v-tabs__item"
  );

  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsendo = await page.$("#search_dashboard");
  await input__slotsendo.click({ clickCount: 1 });
  await input__slotsendo.type("yeaux");
  await page.keyboard.press("Enter");

  //Click Endorse
  await page.waitForSelector(
    "tbody > tr > td > #endorse_endorsed > .v-btn__content"
  );
  await page.click("tbody > tr > td > #endorse_endorsed > .v-btn__content");

  //show dialog
  page.on("dialog", async dialog => {
    await dialog.accept();
  });

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #endorse_endorsed > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//--------Validation for  Department Head choose to Reject the Applicant based on the result of online exam
test("Validation for  Department Head choose to Reject the Applicant based on the result of online exam", async () => {
  await page.waitFor(2000);
  let input__slotsendo;
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Endorsement
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #EndorsedUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #EndorsedUsers > .v-tabs__item"
  );

  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsendo = await page.$("#search_dashboard");
  await input__slotsendo.click({ clickCount: 1 });
  await input__slotsendo.type("TVbW");
  await page.keyboard.press("Enter");

  //Click Reject
  await page.waitForSelector(
    "tbody > tr:nth-child(2) > td > #endorse_reject > .v-btn__content"
  );
  await page.click(
    "tbody > tr:nth-child(2) > td > #endorse_reject > .v-btn__content"
  );

  //Remarks
  await page.click(".v-input #remarks");
  await page.type(".v-input #remarks", "good");

  //Yes Button in Remarks
  await page.waitForSelector(
    ".v-card > .v-card__actions > #setRejectEndorse > #setReject Endorse > .v-btn__content"
  );
  await page.click(
    ".v-card > .v-card__actions > #setRejectEndorse > #setReject Endorse > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-card > .v-card__actions > #setRejectEndorse > #setReject Endorse > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//=================================  A S S E S S M E N T ==================================
//---------------"Validation for Department Head can view list of all applications for ASSESSMENT"------
test("Validation for Department Head can view list of all applications for Assessment", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Assessment
  await page.waitForSelector(
    ".v-tabs__wrapper > .v-tabs__container > #AssessmentUsers > .v-tabs__item > .v-badge"
  );
  await page.click(
    ".v-tabs__wrapper > .v-tabs__container > #AssessmentUsers > .v-tabs__item > .v-badge"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__wrapper > .v-tabs__container > #AssessmentUsers > .v-tabs__item > .v-badge"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//--------------Validation for  Department Head can search for assessment applicant ---------------
test("Validation for  Department Head can search for assessment applicant ", async () => {
  let input__slotsass;
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsass = await page.$("#search_dashboard");
  await input__slotsass.click({ clickCount: 1 });
  await input__slotsass.type("boii");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#search_dashboard").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-----------Validation for Department Head can view Applicants' Resume------------
test("Validation for Department Head can view Applicants' Resume ", async () => {
  // Click View Resume
  await page.waitForSelector(".v-datatable #aview_");
  await page.click(".v-datatable #aview_");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-datatable #aview_").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-----------Validation for Department head can Update the position-----------------------
test("Validation for Department Head can view list of all applications for Assessment", async () => {
  let input__slotsass;
  let input__slotposid;
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Assessment
  await page.waitForSelector(
    ".v-tabs__wrapper > .v-tabs__container > #AssessmentUsers > .v-tabs__item > .v-badge"
  );
  await page.click(
    ".v-tabs__wrapper > .v-tabs__container > #AssessmentUsers > .v-tabs__item > .v-badge"
  );
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsass = await page.$("#search_dashboard");
  await input__slotsass.click({ clickCount: 1 });
  await input__slotsass.type("boii");
  await page.keyboard.press("Enter");

  //update position
  await page.waitForSelector("tbody > tr > td > #aposition_ > .v-btn__content");
  await page.click("tbody > tr > td > #aposition_ > .v-btn__content");

  input__slotposid = await page.$("#positionId");
  await input__slotposid.click({ clickCount: 1 });
  await input__slotposid.type("UX");
  await page.keyboard.press("Enter");

  //Save to update position
  await page.waitForSelector(
    ".v-dialog > .v-card > .v-card__actions > #UpdatePosition > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-card > .v-card__actions > #UpdatePosition > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > #UpdatePosition > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//------------Validation for Department Head can view application form------------------------
test("Validation for Department Head can view application form", async () => {
  let input__slotsass;
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Assessment
  await page.waitForSelector(
    ".v-tabs__wrapper > .v-tabs__container > #AssessmentUsers > .v-tabs__item > .v-badge"
  );
  await page.click(
    ".v-tabs__wrapper > .v-tabs__container > #AssessmentUsers > .v-tabs__item > .v-badge"
  );
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsass = await page.$("#search_dashboard");
  await input__slotsass.click({ clickCount: 1 });
  await input__slotsass.type("boii");
  await page.keyboard.press("Enter");

  //View application form
  await page.waitForSelector(
    "tbody > tr > td > #endorse_exam > .v-btn__content"
  );
  await page.click("tbody > tr > td > #endorse_exam > .v-btn__content");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > #UpdatePosition > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-------Validation for Department Head choose to Rate the applicant Using Interview Assessment Form--------
test("Validation for Department Head choose to Rate the applicant Using Interview Assessment Form ", async () => {
  // Click View IAF
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Assessment
  await page.waitForSelector(
    ".v-tabs__wrapper > .v-tabs__container > #AssessmentUsers > .v-tabs__item > .v-badge"
  );
  await page.click(
    ".v-tabs__wrapper > .v-tabs__container > #AssessmentUsers > .v-tabs__item > .v-badge"
  );
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsass = await page.$("#search_dashboard");
  await input__slotsass.click({ clickCount: 1 });
  await input__slotsass.type("boii");
  await page.keyboard.press("Enter");

  //click rate IAF
  await page.waitForSelector("tbody > tr > td > #arateiaf > .v-btn__content");
  await page.click("tbody > tr > td > #arateiaf > .v-btn__content");
  await page.waitForSelector(
    ".v-input--is-label-active > .v-input__control > .v-input__slot > .v-text-field__slot > input"
  );
  await page.click(
    ".v-input--is-label-active > .v-input__control > .v-input__slot > .v-text-field__slot > input"
  );

  await page.waitForSelector(
    "tbody > tr > td > .v-btn--active > .v-btn__content"
  );
  await page.click("tbody > tr > td > .v-btn--active > .v-btn__content");

  await page.waitForSelector(
    ".v-dialog > .v-picker > .v-picker__actions > #dateSave > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-picker > .v-picker__actions > #dateSave > .v-btn__content"
  );

  await page.waitForSelector(
    ".container:nth-child(7) > b > #communication_skills > .v-input__control > .v-input__slot > .v-input--radio-group__input > .flex:nth-child(2) > .v-radio > .v-input--selection-controls__input > .v-input--selection-controls__ripple"
  );
  await page.click(
    ".container:nth-child(7) > b > #communication_skills > .v-input__control > .v-input__slot > .v-input--radio-group__input > .flex:nth-child(2) > .v-radio > .v-input--selection-controls__input > .v-input--selection-controls__ripple"
  );

  await page.waitForSelector(
    ".container:nth-child(7) > b > #confidence > .v-input__control > .v-input__slot > .v-input--radio-group__input > .flex:nth-child(2) > .v-radio > .v-input--selection-controls__input > .v-input--selection-controls__ripple"
  );
  await page.click(
    ".container:nth-child(7) > b > #confidence > .v-input__control > .v-input__slot > .v-input--radio-group__input > .flex:nth-child(2) > .v-radio > .v-input--selection-controls__input > .v-input--selection-controls__ripple"
  );

  await page.waitForSelector(
    ".container:nth-child(7) > b > #physical_appearance > .v-input__control > .v-input__slot > .v-input--radio-group__input > .flex:nth-child(2) > .v-radio > .v-input--selection-controls__input > .v-input--selection-controls__ripple"
  );
  await page.click(
    ".container:nth-child(7) > b > #physical_appearance > .v-input__control > .v-input__slot > .v-input--radio-group__input > .flex:nth-child(2) > .v-radio > .v-input--selection-controls__input > .v-input--selection-controls__ripple"
  );

  await page.waitForSelector(
    ".container:nth-child(7) > b > # knowledge_skills > .v-input__control > .v-input__slot > .v-input--radio-group__input > .flex:nth-child(2) > .v-radio > .v-input--selection-controls__input > .v-input--selection-controls__ripple"
  );
  await page.click(
    ".container:nth-child(7) > b > # knowledge_skills > .v-input__control > .v-input__slot > .v-input--radio-group__input > .flex:nth-child(2) > .v-radio > .v-input--selection-controls__input > .v-input--selection-controls__ripple"
  );

  await page.click(".v-input #asking_rate");
  await page.type(".v-input #asking_rate", "15000");

  await page.click(".v-input #availability");
  await page.type(".v-input #availability", "10000");

  await page.click(".v-input #gen_remarks_recommendations");
  await page.type(".v-input #gen_remarks_recommendations", "Good");

  //Save IAF
  await page.waitForSelector(".v-dialog__content #rateIAFSave");
  await page.click(".v-dialog__content #rateIAFSave");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-dialog__content #rateIAFSave")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-------Validation for Department Head choose to Reject the Applicant based on the Assessment--------
test("Validation for Department Head choose to Reject the Applicant based on the Assessment ", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Assessment
  await page.waitForSelector(
    ".v-tabs__wrapper > .v-tabs__container > #AssessmentUsers > .v-tabs__item > .v-badge"
  );
  await page.click(
    ".v-tabs__wrapper > .v-tabs__container > #AssessmentUsers > .v-tabs__item > .v-badge"
  );
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsass = await page.$("#search_dashboard");
  await input__slotsass.click({ clickCount: 1 });
  await input__slotsass.type("qwar");
  await page.keyboard.press("Enter");

  //click reject button
  await page.waitForSelector("tbody > tr > td > #areject_ > .v-btn__content");
  await page.click("tbody > tr > td > #areject_ > .v-btn__content");

  await page.click(".v-input #remarks");
  await page.type(".v-input #remarks", "good");

  //Save button
  await page.waitForSelector(".v-dialog #setRejectAsses > #setRejectAsses");
  await page.click(".v-dialog #setRejectAsses > #setRejectAsses");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog #setRejectAsses > #setRejectAsses"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//============================  P E N D I N G   F O R   R E V I E W =========================================
//---------Validation for Department Head can view list of all applications' pending for Review--------
test("Validation for Department Head can view list of all applications' pending for Review", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Pending for Review
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #PendingUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #PendingUsers > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #PendingUsers > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------------Validation for Department Head can search for pending for review applicant----------------
test("Validation for  Department Head can search for assessment applicant ", async () => {
  let input__slotsapp;
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsapp = await page.$("#search_dashboard");
  await input__slotsapp.click({ clickCount: 1 });
  await input__slotsapp.type("AaIU");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#search_dashboard").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//---------------Validation for  Department Head wants to view Applicants' Resume--------------
test("Validation for  Department Head wants to view Applicants' Resume", async () => {
  // Click View Resume
  await page.waitForSelector(".v-datatable #pview_");
  await page.click(".v-datatable #pview_");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-datatable #pview_").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//------------Validation for Department Head choose to view Interview Assessment Form of the applicant------
test("Validation for Department Head choose to view Interview Assessment Form of the applicant", async () => {
  page = await browser.newPage();
  let input__slotsapp;
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Pending for Review
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #PendingUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #PendingUsers > .v-tabs__item"
  );
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsapp = await page.$("#search_dashboard");
  await input__slotsapp.click({ clickCount: 1 });
  await input__slotsapp.type("AaIU");
  await page.keyboard.press("Enter");

  //View IAF
  await page.waitForSelector("tbody > tr > td > #viewIAF > .v-btn__content");
  await page.click("tbody > tr > td > #viewIAF > .v-btn__content");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #viewIAF > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  //CLOSE
  await page.waitForSelector(".v-dialog__content #ViewIAFCancel");
  await page.click(".v-dialog__content #ViewIAFCancel");
}, 200000);

//-----------------Validation for Department Head wants to edit the IAF of the applicant--------------
test("Validation for Department Head wants to edit the IAF of the applicant", async () => {
  //Edit IAF
  await page.waitForSelector(
    "tbody > tr:nth-child(1) > td > #editIAF > .v-btn__content"
  );
  await page.click("tbody > tr:nth-child(1) > td > #editIAF > .v-btn__content");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr:nth-child(1) > td > #editIAF > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//---------------Validation for  Department Head want to save the updated IAF of the applicant--------------
test("Validation for  Department Head want to save the updated IAF of the applicant", async () => {
  //Edit IAF
  await page.waitForSelector(
    "tbody > tr:nth-child(1) > td > #editIAF > .v-btn__content"
  );
  await page.click("tbody > tr:nth-child(1) > td > #editIAF > .v-btn__content");

  await page.click(".v-input--is-focused #viewremarks");
  await page.type(".v-input--is-focused #viewremarks", "good");

  //save button
  await page.waitForSelector(".v-dialog__content #EditIAFSave");
  await page.click(".v-dialog__content #EditIAFSave");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-dialog__content #EditIAFSave")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-------Validation for Department Head choose to Kept for reference the Applicant based on the result----
test("Validation for Department Head choose to Kept for reference the Applicant based on the result", async () => {
  //click kept for reference
  await page.waitForSelector(
    "tbody > tr:nth-child(1) > td > #pforref_ > .v-btn__content"
  );
  await page.click(
    "tbody > tr:nth-child(1) > td > #pforref_ > .v-btn__content"
  );

  //Show dialog box
  page.on("dialog", async dialog => {
    await dialog.accept();
  });

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr:nth-child(1) > td > #pforref_ > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//----------Validation for Department Head choose to Shortlist the Applicant based on the result---------
test("Validation for Department Head choose to Shortlist the Applicant based on the result", async () => {
  let input__slotsapp;
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Pending for Review
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #PendingUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #PendingUsers > .v-tabs__item"
  );
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsapp = await page.$("#search_dashboard");
  await input__slotsapp.click({ clickCount: 1 });
  await input__slotsapp.type("xlPW");
  await page.keyboard.press("Enter");

  //Shortlisted
  await page.waitForSelector(
    "tbody > tr > td > #pshortlist_ > .v-btn__content"
  );
  await page.click("tbody > tr > td > #pshortlist_ > .v-btn__content");

  //Show dialog box
  page.on("dialog", async dialog => {
    await dialog.accept();
  });
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #pshortlist_ > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//---------Validation for Department Head choose to Reject the Applicant based on the result------------
test("Validation for Department Head choose to Reject the Applicant based on the result", async () => {
  page = await browser.newPage();
  let input__slotsapp;
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Pending for Review
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #PendingUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #PendingUsers > .v-tabs__item"
  );
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsapp = await page.$("#search_dashboard");
  await input__slotsapp.click({ clickCount: 1 });
  await input__slotsapp.type("boii");
  await page.keyboard.press("Enter");

  //reject button
  await page.waitForSelector(
    "tbody > tr:nth-child(2) > td > #preject_ > .v-btn__content"
  );
  await page.click(
    "tbody > tr:nth-child(2) > td > #preject_ > .v-btn__content"
  );

  //Click Remarks
  await page.click(".v-input #remarks");
  await page.type(".v-input #remarks", "not good");

  //Save Button
  await page.waitForSelector(
    ".v-card > .v-card__actions > #setRejectPending > #setRejectPending > .v-btn__content"
  );
  await page.click(
    ".v-card > .v-card__actions > #setRejectPending > #setRejectPending > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-card > .v-card__actions > #setRejectPending > #setRejectPending > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//==================================  S H O R T L I S T E D   ==========================================
//-------------Validation for Department Head can view list of all applicant in Shortlisted------------
test("Validation for Department Head can view list of all applicant in Shortlisted", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Shortlisted
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #ShortlistedUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #ShortlistedUsers > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #ShortlistedUsers > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//---------Validation for Department Head can search for applicants' Kept for refrence---------
test("Validation for Department Head can search for applicants' Kept for refrence", async () => {
  let input__slotsapp;
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsapp = await page.$("#search_dashboard");
  await input__slotsapp.click({ clickCount: 1 });
  await input__slotsapp.type("AKsQ");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#search_dashboard").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//---------Validation for Department Head wants to view Applicants' Resume-----------------
test("Validation for Department Head wants to view Applicants' Resume", async () => {
  // Click View Resume
  await page.waitForSelector(".v-datatable #shortlisted_view");
  await page.click(".v-datatable #shortlisted_view");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-datatable #shortlisted_view")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//========================== K E P T  F O R  R E F E R E N C E ====================================
//------Validation for Department Head can view list of all applications' Kept for reference-------
test("Validation for Department Head can view list of all applications' Kept for reference", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Kept for reference
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #KeptForReferenceUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #KeptForReferenceUsers > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #KeptForReferenceUsers > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//---------Validation for Department Head can search for applicants' Kept for refrence---------
test("Validation for Department Head can search for applicants' Kept for refrence", async () => {
  let input__slotsapp;
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsapp = await page.$("#search_dashboard");
  await input__slotsapp.click({ clickCount: 1 });
  await input__slotsapp.type("civodohe");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#search_dashboard").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//---------Validation for Department Head wants to view Applicants' Resume-----------------
test("Validation for Department Head wants to view Applicants' Resume", async () => {
  // Click View Resume
  await page.waitForSelector(".v-datatable #rview_");
  await page.click(".v-datatable #rview_");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-datatable #rview_").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-----------Validation for Department Head wants to put back the applicant on assessment--------
test("Validation for Department Head can view list of all applications' Kept for reference", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Kept for reference
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #KeptForReferenceUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #KeptForReferenceUsers > .v-tabs__item"
  );

  //Search
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsapp = await page.$("#search_dashboard");
  await input__slotsapp.click({ clickCount: 1 });
  await input__slotsapp.type("civodohe");
  await page.keyboard.press("Enter");

  //click assessment
  await page.waitForSelector("tbody > tr > td > #rassess_ > .v-btn__content");
  await page.click("tbody > tr > td > #rassess_ > .v-btn__content");

  //Show dialog box
  page.on("dialog", async dialog => {
    await dialog.accept();
  });

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #rassess_ > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//==================================  D E P L O Y E D  ==========================================
//-------------Validation for Department Head can view list of all applicant in Deployed------------
test("Validation for Department Head can view list of all applicant in Deloyed", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard");
  await page.setViewport({ width: 1366, height: 768 });

  //Deployed
  await page.waitForSelector(
    ".v-tabs__wrapper > .v-tabs__container > #Deploy > .v-tabs__item > .v-badge"
  );
  await page.click(
    ".v-tabs__wrapper > .v-tabs__container > #Deploy > .v-tabs__item > .v-badge"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__wrapper > .v-tabs__container > #Deploy > .v-tabs__item > .v-badge"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//---------Validation for Department Head can search for applicants' Kept for refrence---------
test("Validation for Department Head can search for applicants' Kept for refrence", async () => {
  let input__slotsapp;
  //Search Bar
  await page.waitForSelector("input#search_dashboard");
  await page.click("input#search_dashboard");

  input__slotsapp = await page.$("#search_dashboard");
  await input__slotsapp.click({ clickCount: 1 });
  await input__slotsapp.type("bhVe");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#search_dashboard").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//---------Validation for Department Head wants to view Applicants' Resume-----------------
test("Validation for Department Head wants to view Applicants' Resume", async () => {
  // Click View Resume

  await page.waitForSelector(
    "tbody > tr > td > #shortlisted_view > .v-btn__content"
  );
  await page.click("tbody > tr > td > #shortlisted_view > .v-btn__content");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-datatable #shortlisted_view")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//====================================  R E P O R T S ================================================
//--------Validation for Hiring Officer wants to view reports of Kept for Reference of applicants--------
test("Validation for User wants to view reports of Kept for Reference of applicants", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //Reports
  await page.waitForSelector(
    ".v-responsive__content > .v-list > .v-list-item > #sidebar5 > .v-list__tile__title"
  );
  await page.click(
    ".v-responsive__content > .v-list > .v-list-item > #sidebar5 > .v-list__tile__title"
  );
  //Kept for Reference
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #keptforRef > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #keptforRef > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #keptforRef > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-----------Validation for Hiring Officer (PG) can search for applicant in Kept for Reference-----------
test("Validation for Hiring Officer (PG) can search for applicant in Kept for Reference", async () => {
  //Search
  await page.waitForSelector("input#searhReports");
  await page.click("input#searhReports");

  input__slotsrec = await page.$("#searhReports");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("civodohe");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#searhReports").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitForSelector("button#search");
  await page.click("button#search");
}, 200000);

//------------------Validation for User wants to view reports in Deployed of applicants--------------
test("Validation for User wants to view reports in Deployed of applicants", async () => {
  //Deployed
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #deplyed > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #deplyed > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #deplyed > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-----------Validation for Hiring Officer (PG) can search for applicant in Deployed-----------
test("Validation for Hiring Officer (PG) can search for applicant in Deployed", async () => {
  //Search
  await page.waitForSelector("input#searhReports");
  await page.click("input#searhReports");

  input__slotsrec = await page.$("#searhReports");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("aEHf");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#searhReports").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  //clear search
  await page.waitForSelector("button#search");
  await page.click("button#search");
}, 200000);

//------------------Validation for User wants to view reports in Blacklisted of applicants--------------
test("Validation for User wants to view reports in Blacklisted of applicants", async () => {
  //blacklisted
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #blacklisted > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #blacklisted > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #blacklisted > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);
//-----------Validation for Hiring Officer (PG) can search for applicant in Deployed-----------
test("Validation for Hiring Officer (PG) can search for applicant in Deployed", async () => {
  //Search
  await page.waitForSelector("input#searhReports");
  await page.click("input#searhReports");

  input__slotsrec = await page.$("#searhReports");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("IOLu");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#searhReports").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  //clear search
  await page.waitForSelector("button#search");
  await page.click("button#search");
}, 200000);

//------------------Validation for User wants to view reports in Rejected of applicants--------------
test("Validation for User wants to view reports in Rejected of applicants", async () => {
  //Rejected
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #rected > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #rected > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #rected > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-----------Validation for Hiring Officer (PG) can search for applicant in Deployed-----------
test("Validation for Hiring Officer (PG) can search for applicant in Deployed", async () => {
  //Search
  await page.waitForSelector("input#searhReports");
  await page.click("input#searhReports");

  input__slotsrec = await page.$("#searhReports");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("FMpG");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#searhReports").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  //clear search
  await page.waitForSelector("button#search");
  await page.click("button#search");
  await browser.close();
}, 200000);
