const randomstring = require("randomstring");
const puppeteer = require("puppeteer");
let page;
let browser;
//Variables
// random first name
const fname = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Middle Name
const mname = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Last Name
const lname = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Email
const email = randomstring.generate({
  length: 4,
  charset: "alphabetic"
});
//random Mobile number
const mobile = randomstring.generate({
  length: 11,
  charset: "numeric"
});
//========================= L O G I N  A S  H I R I N G  O F F I C E R (PG)   =============================
test("Login as HIRING OFFICER (PG)", async () => {
  browser = await puppeteer.launch({
    executablePath:
      "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
    headless: true,
    slowMo: 30,
    args: ["--proxy-server= 172.16.1.6:3128"]
  });
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/login");
  await page.setViewport({ width: 1366, height: 768 });
  //Input Username
  await page.click(".elevation-12 #email");
  await page.type(".elevation-12 #email", "pg.jammel@gmail.com");

  //Input Password
  await page.click(".elevation-12 #password");
  await page.type(".elevation-12 #password", "123456789");

  //Login Button
  await page.waitForSelector(".elevation-12 #loginButton");
  await page.click(".elevation-12 #loginButton");

  //dashboard
  await page.waitForSelector(
    ".v-responsive__content > .v-list > .v-list-item > #sidebar1 > .v-list__tile__title"
  );
  await page.click(
    ".v-responsive__content > .v-list > .v-list-item > #sidebar1 > .v-list__tile__title"
  );
  //add applicant
  await page.waitForSelector(".v-responsive #sidebar9");
  await page.click(".v-responsive #sidebar9");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-responsive #sidebar9").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 2000000);

//==============================  A D D   A P P L I C A N T ======================================
test("Add applicant", async () => {
  let input__slotapos;
  let input__slotadep;

  // DepartmentID
  input__slotadep = await page.$("#deptId");
  await input__slotadep.click({ clickCount: 1 });
  await input__slotadep.type("BFI");
  await page.keyboard.press("Enter");

  // POSITION COMBOBOX
  input__slotapos = await page.$("#positionId");
  await input__slotapos.click({ clickCount: 1 });
  await input__slotapos.type("dev");
  await page.keyboard.press("Enter");

  //auto email
  await page.click(".v-input #email");
  await page.type(".v-input #email", `${email}@getnada.com`);

  //first name
  await page.click(".v-input #fname");
  await page.type(".v-input #fname", `${fname}`);
  //middle name

  await page.click(".v-input #mname");
  await page.type(".v-input #mname", `${mname}`);

  //lastname
  await page.click(".v-input #lname");
  await page.type(".v-input #lname", `${lname}`);

  //Religion
  await page.click(".v-input #religion");
  await page.type(".v-input #religion", "Roman Catholic");

  //Mobile Number
  await page.click(".v-input #mobile");
  await page.type(".v-input #mobile", `${mobile}`);

  //Livestock
  await page.click(".v-input #livestock");
  await page.type(".v-input #livestock", "N/A");

  //Are you willing to Travel
  input__slotapos = await page.$("#travel");
  await input__slotapos.click({ clickCount: 1 });
  await input__slotapos.type("YES");
  await page.keyboard.press("Enter");

  const fileInput = await page.$("input[type=file]");
  await fileInput.uploadFile("D:/New folder/1.png");

  //SAVE
  await page.waitForSelector('.v-card #addapplicant')
  await page.click('.v-card #addapplicant')

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-card #addapplicant").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await browser.close();
  await page.waitFor(2000);
}, 2000000);
