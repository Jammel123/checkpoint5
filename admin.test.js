//----------------   VIEW ALL USER    --------------
const randomstring = require("randomstring");
const puppeteer = require("puppeteer");
let page;
let browser;
//Variables

// random department name
const dname = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
// random department desc
const desc = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
// random first name
const fname = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Middle Name
const mn = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Last Name
const ln = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Email
const email = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Mobile number
const mobile = randomstring.generate({
  length: 11,
  charset: "numeric"
});
// --------------- VIEW ALL USERS ------
test("Validation for View all Users", async () => {
  browser = await puppeteer.launch({
    headless: true,
    slowMo: 30,
    args: ["--proxy-server= 172.16.1.6:3128", "--start-fullscreen"]
  });
  page = await browser.newPage();

  await page.goto("http://rectrack.jltechsol.com/login");

  await page.setViewport({ width: 1366, height: 768 });

  //Input Username
  await page.click(".elevation-12 #email");
  await page.type(".elevation-12 #email", "admin.jammel@gmail.com");

  //Input Password
  await page.click(".elevation-12 #password");
  await page.type(".elevation-12 #password", "123456789");

  //Login Button
  await page.waitForSelector(
    ".card-body > .mt-5 > #loginButton > small > span"
  );
  await page.click(".card-body > .mt-5 > #loginButton > small > span");

  await page.waitFor(2000);
  //Side Bar View User

  await page.waitForSelector(
    ".v-responsive__content > .v-list > .v-list-item:nth-child(7) > #sidebar > .v-list__tile__title"
  );
  await page.click(
    ".v-responsive__content > .v-list > .v-list-item:nth-child(7) > #sidebar > .v-list__tile__title"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-responsive__content > .v-list > .v-list-item:nth-child(7) > #sidebar > .v-list__tile__title"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-------------------   ADD USERS    ----------------------

test("Validation for Add USER", async () => {
  let input__slotupos;
  let input__sloturole;

  //Add User
  await page.waitForSelector(".layout #addUser");
  await page.click(".layout #addUser");

  // First Name
  await page.click(".v-input #fn");
  await page.type(".v-input #fn", `${fname}`);
  //MIDDLE NAME
  await page.click(".v-input #mn");
  await page.type(".v-input #mn", `${mn}`);
  //LASTNAME
  await page.click(".v-input #ln");
  await page.type(".v-input #ln", `${ln}`);
  //EMAIL
  await page.click(".v-input #email");
  await page.type(".v-input #email", `${email}@gmail.com`);

  //Password
  await page.click(".v-input #pw1");
  await page.type(".v-input #pw1", "123456789");
  //Password
  await page.click(".v-input #pw2");
  await page.type(".v-input #pw2", "123456789");

  // POSITION COMBOBOX
  input__slotupos = await page.$("#positionId");
  await input__slotupos.click({ clickCount: 1 });
  await input__slotupos.type("UX");
  await page.keyboard.press("Enter");

  //ROLE COMBOBOX
  input__sloturole = await page.$("#roleItems");
  await input__sloturole.click({ clickCount: 1 });
  await input__sloturole.type("dp");
  await page.keyboard.press("Enter");

  //Input Mobile Number
  await page.click(".v-input #mobile");
  await page.type(".v-input #mobile", `${mobile}`);

  //Save Button
  await page.waitForSelector(".v-dialog #addUserDialogSave");
  await page.click(".v-dialog #addUserDialogSave");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-dialog #addUserDialogSave")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//---------------   SEARCH USER BY (NAME, DEPARTMENT) -----------

test("Validation for Search User", async () => {
  let input__slotsu;
  //Click Search Bar
  await page.waitForSelector(".my-3 > #search #searchInputUsers");
  await page.click(".my-3 > #search #searchInputUsers");

  //Click Search User
  input__slotsu = await page.$("#searchInputUsers");
  await input__slotsu.click({ clickCount: 1 });
  await input__slotsu.type(`${fname}`);
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".my-3 > #search #searchInputUsers")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//--------------   VIEW USER INFO ----------

test("validation for View User Info", async () => {
  await page.waitFor(2000);
  //Click View User Info
  await page.waitForSelector(".v-datatable #viewUserDialog");
  await page.click(".v-datatable #viewUserDialog");

  await page.waitFor(2000);

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-datatable #viewUserDialog")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();

  //close button
  await page.waitForSelector(
    ".v-dialog__content:nth-child(2) > .v-dialog > .v-card > .v-card__actions > .v-btn"
  );
  await page.click(
    ".v-dialog__content:nth-child(2) > .v-dialog > .v-card > .v-card__actions > .v-btn"
  );
  await page.waitFor(2000);
}, 200000);

// ------------ EDIT USER INFO  ------------------

test("Validation for Edit User Info", async () => {
  // let input__slotupos;
  // let input__sloturole;

  //Click Edit User info
  await page.waitForSelector(
    "tbody > tr > td > #editUserDialog > .v-btn__content"
  );
  await page.click("tbody > tr > td > #editUserDialog > .v-btn__content");

  //Input First Name
  await page.waitForSelector(".v-input #fn");
  await page.click(".v-input #fn");
  //Input Middle Name
  await page.waitForSelector(".v-input #mn");
  await page.click(".v-input #mn");
  //Input Last name
  await page.waitForSelector(".v-input #ln");
  await page.click(".v-input #ln");
  //Input Email
  await page.waitForSelector(".v-input #email");
  await page.click(".v-input #email");
  //Password
  await page.click(".v-input #pw1");
  await page.type(".v-input #pw1", "123456789");
  //Password
  await page.click(".v-input #pw2");
  await page.type(".v-input #pw2", "123456789");

  // // POSITION COMBOBOX
  // input__slotupos = await page.$("#positionId");
  // await input__slotupos.click({ clickCount: 1 });
  // await input__slotupos.type("SQA");
  // await page.keyboard.press("Enter");

  // //ROLE COMBOBOX
  // input__sloturole = await page.$("#roleItems");
  // await input__sloturole.click({ clickCount: 1 });
  // await input__sloturole.type("Admin");
  // await page.keyboard.press("Enter");

  // //clear fields
  // await page.focus(".v-input #mobile");
  // await page.keyboard.down("Control");
  // await page.keyboard.press("A");
  // await page.keyboard.up("Control");
  // await page.keyboard.press("Backspace");
  // await page.keyboard.type("09856231546");

  //Save Button
  await page.waitForSelector(
    ".v-card > .v-card__actions > div > #editUserDialogSave > .v-btn__content"
  );
  await page.click(
    ".v-card > .v-card__actions > div > #editUserDialogSave > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-card > .v-card__actions > div > #editUserDialogSave > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

// -------------- VIEW ALL DEPARTMENT-----------------

test("Validation for View All Department", async () => {
  //Side bar Department
  await page.waitForSelector(
    ".v-responsive__content > .v-list > .v-list-item:nth-child(3) > #sidebar > .v-list__tile__title"
  );
  await page.click(
    ".v-responsive__content > .v-list > .v-list-item:nth-child(3) > #sidebar > .v-list__tile__title"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-responsive__content > .v-list > .v-list-item:nth-child(3) > #sidebar > .v-list__tile__title"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//------------    ADD DEPARTMENT   --------------

test("Validation for Add Department", async () => {
  //Add Department Button
  await page.waitForSelector(".layout #addDepartment");
  await page.click(".layout #addDepartment");

  //Input Department name
  await page.click(".v-input #dname");
  await page.type(".v-input #dname", `${dname}`);

  //Input Description
  await page.click(".v-input #desc");
  await page.type(".v-input #desc", `${desc}`);

  //Save Button
  await page.waitForSelector(".v-dialog #dialogAddSaveDepartment");
  await page.click(".v-dialog #dialogAddSaveDepartment");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-dialog #dialogAddSaveDepartment")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toEqual("Save");
  await page.waitFor(2000);
}, 200000);

//--------------- SEARCH DEPARTMENT BY NAME ---------------
test("Validation for Search Department by Name", async () => {
  let input__slotsdep;
  await page.waitForSelector(".my-3 > #search #searchInputDepartment");
  await page.click(".my-3 > #search #searchInputDepartment");

  //Click Search User
  input__slotsdep = await page.$("#searchInputDepartment");
  await input__slotsdep.click({ clickCount: 1 });
  await input__slotsdep.type(`${dname}`);
  await page.keyboard.press("Enter");
  await page.waitFor(2000);
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".my-3 > #search #searchInputDepartment"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------- VIEW DEPARTMENT INFO ---------------
test("Validation for View Department Info", async () => {
  //Click View Button
  await page.waitForSelector(
    "tr > td > #ViewDepartmentDetails > .v-btn__content > span"
  );
  await page.click("tr > td > #ViewDepartmentDetails > .v-btn__content > span");

  await page.waitFor(2000);

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tr > td > #ViewDepartmentDetails > .v-btn__content > span"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();

  // Close View Button
  await page.waitForSelector(
    ".v-dialog__content:nth-child(2) > .v-dialog > .v-card > .v-card__actions > .v-btn"
  );
  await page.click(
    ".v-dialog__content:nth-child(2) > .v-dialog > .v-card > .v-card__actions > .v-btn"
  );

  await page.waitFor(2000);
}, 200000);

// --------------- EDIT DEPARTMENT INFO ---------------
test("Validation for Edit Department Info", async () => {
  //Click Edit Button
  await page.waitForSelector(
    "tr > td > #editDepartmentDetails > .v-btn__content > .v-icon"
  );
  await page.click(
    "tr > td > #editDepartmentDetails > .v-btn__content > .v-icon"
  );

  // Click Edit Description
  await page.waitForSelector(".v-input #desc");
  await page.click(".v-input #desc");
  await page.focus(".v-input #desc");
  await page.keyboard.down("Control");
  await page.keyboard.press("A");
  await page.keyboard.up("Control");
  await page.keyboard.press("Backspace");
  await page.keyboard.type(`${desc}s`);

  await page.waitForSelector(
    ".v-card > .v-card__actions > div > #dialogUpdateDepartment > .v-btn__content"
  );
  await page.click(
    ".v-card > .v-card__actions > div > #dialogUpdateDepartment > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-card > .v-card__actions > div > #dialogUpdateDepartment > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toEqual("Save");
  await page.waitFor(2000);
}, 200000);

//--------------------VIEW SYSTEM LOGS---------------------
test("Validation for View System Logs", async () => {
  let input__slotdfilt;
  await page.waitForSelector(
    ".v-responsive__content > .v-list > .v-list-item:nth-child(8) > #sidebar > .v-list__tile__title"
  );
  await page.click(
    ".v-responsive__content > .v-list > .v-list-item:nth-child(8) > #sidebar > .v-list__tile__title"
  );

  //Click Datefilter
  await page.waitForSelector(".my-3 > #filter #dateFilter");
  await page.click(".my-3 > #filter #dateFilter");
  input__slotdfilt = await page.$("#dateFilter");
  await input__slotdfilt.click({ clickCount: 1 });
  await input__slotdfilt.type("05/09/2019");
  await page.keyboard.press("Enter");

  //View Button
  await page.waitForSelector(".my-3 > #filter #view");
  await page.click(".my-3 > #filter #view");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".my-3 > #filter #view").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
  await browser.close();
}, 2000000);
