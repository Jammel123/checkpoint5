//---------VALID USERNAME AND PASSWORD---------
const puppeteer = require("puppeteer");
let page;
let browser;
test("valid Username and Password", async () => {
  browser = await puppeteer.launch({
    headless: true,
    slowMo: 30,
    args: ["--proxy-server= 172.16.1.6:3128","--start-fullscreen"]
    //, "--start-fullscreen"
  });
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/login");

  await page.setViewport({ width: 1366, height: 768 });

  //Input Username
  await page.click(".elevation-12 #email");
  await page.type(".elevation-12 #email", "admin.jammel@gmail.com");

  //Input Password
  await page.click(".elevation-12 #password");
  await page.type(".elevation-12 #password", "123456789");

  //Login Button
  await page.waitForSelector(
    ".card-body > .mt-5 > #loginButton > small > span"
  );
  await page.click(".card-body > .mt-5 > #loginButton > small > span");

  //Side Bar Department
  await page.waitForSelector(
    ".v-responsive__content > .v-list > .v-list-item:nth-child(3) > #sidebar > .v-list__tile__title"
  );
  await page.click(
    ".v-responsive__content > .v-list > .v-list-item:nth-child(3) > #sidebar > .v-list__tile__title"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-responsive__content > .v-list > .v-list-item:nth-child(3) > #sidebar > .v-list__tile__title"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();

  await page.waitForSelector("#core-toolbar #logout");
  await page.click("#core-toolbar #logout");

  await page.waitForSelector(
    "#modal > .sweet-modal > .sweet-content > .sweet-content-content > .btn-success"
  );
  await page.click(
    "#modal > .sweet-modal > .sweet-content > .sweet-content-content > .btn-success"
  );

  await page.waitFor(2000);
}, 200000);

//---------INVALID USERNAME AND VALID PASSWORD---------

test("Invalid Username and Valid Password", async () => {
  //Input Username
  await page.click(".elevation-12 #email");
  await page.type(".elevation-12 #email", "jammel@biotechfarms.net");

  //Input Password
  await page.click(".elevation-12 #password");
  await page.type(".elevation-12 #password", "123456789");

  //Login Button
  await page.waitForSelector(
    ".card-body > .mt-5 > #loginButton > small > span"
  );
  await page.click(".card-body > .mt-5 > #loginButton > small > span");

  await page.waitForSelector(
    ".card-body > .mt-5 > .v-snack > .v-snack__wrapper > .v-snack__content"
  );
  await page.click(
    ".card-body > .mt-5 > .v-snack > .v-snack__wrapper > .v-snack__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".card-body > .mt-5 > .v-snack > .v-snack__wrapper > .v-snack__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toEqual("Invalid Account..");
}, 200000);

//---------VALID USERNAME AND INVALID PASSWORD---------

test("Valid Username and Invalid Password", async () => {
  page = await browser.newPage();

  await page.goto("http://rectrack.jltechsol.com/login");

  await page.setViewport({ width: 1366, height: 768 });

  //Input Username
  await page.click(".elevation-12 #email");
  await page.type(".elevation-12 #email", "jammel.milan@biotechfarms.net");

  //Input Password
  await page.click(".elevation-12 #password");
  await page.type(".elevation-12 #password", "asdasd");

  //Login Button
  await page.waitForSelector(
    ".card-body > .mt-5 > #loginButton > small > span"
  );
  await page.click(".card-body > .mt-5 > #loginButton > small > span");

  await page.waitForSelector(
    ".card-body > .mt-5 > .v-snack > .v-snack__wrapper > .v-snack__content"
  );
  await page.click(
    ".card-body > .mt-5 > .v-snack > .v-snack__wrapper > .v-snack__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".card-body > .mt-5 > .v-snack > .v-snack__wrapper > .v-snack__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toEqual("Invalid Account..");
}, 200000);

//---------INVALID USERNAME AND INVALID PASSWORD---------

test("Invalid Username and Invalid Password", async () => {
  page = await browser.newPage();

  await page.goto("http://rectrack.jltechsol.com/login");

  await page.setViewport({ width: 1366, height: 768 });

  //Input Username
  await page.click(".elevation-12 #email");
  await page.type(".elevation-12 #email", "jammel.milansdas@biotechfarms.net");

  //Input Password
  await page.click(".elevation-12 #password");
  await page.type(".elevation-12 #password", "asdassdad");

  //Login Button
  await page.waitForSelector(
    ".card-body > .mt-5 > #loginButton > small > span"
  );
  await page.click(".card-body > .mt-5 > #loginButton > small > span");

  await page.waitForSelector(
    ".card-body > .mt-5 > .v-snack > .v-snack__wrapper > .v-snack__content"
  );
  await page.click(
    ".card-body > .mt-5 > .v-snack > .v-snack__wrapper > .v-snack__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".card-body > .mt-5 > .v-snack > .v-snack__wrapper > .v-snack__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toEqual("Invalid Account..");
}, 200000);

//---------NULL USERNAME AND NULL PASSWORD---------

test("Null Username and Null Password", async () => {
  page = await browser.newPage();

  await page.goto("http://rectrack.jltechsol.com/login");

  await page.setViewport({ width: 1366, height: 768 });

  //Click Username
  await page.waitForSelector(".elevation-12 #email");
  await page.click(".elevation-12 #email");
  //Click Password
  await page.waitForSelector(".elevation-12 #password");
  await page.click(".elevation-12 #password");
  //Click Login Button
  await page.waitForSelector(
    ".card-body > .mt-5 > #loginButton > small > span"
  );
  await page.click(".card-body > .mt-5 > #loginButton > small > span");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".elevation-12 #loginButton")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toEqual("LOGIN");
}, 200000);

//---------NULL USERNAME AND VALID PASSWORD---------

test("Null Username and Valid Password", async () => {
  page = await browser.newPage();

  await page.goto("http://rectrack.jltechsol.com/login");

  await page.setViewport({ width: 1366, height: 768 });

  //Click Username
  await page.waitForSelector(".elevation-12 #email");
  await page.click(".elevation-12 #email");
  //Click Password
  await page.click(".elevation-12 #password");
  await page.type(".elevation-12 #password", "123456789");
  //Click Login Button
  await page.waitForSelector(
    ".card-body > .mt-5 > #loginButton > small > span"
  );
  await page.click(".card-body > .mt-5 > #loginButton > small > span");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".elevation-12 #loginButton")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toEqual("LOGIN");
}, 200000);

//---------VALID USERNAME AND NULL PASSWORD---------

test("Valid Username and Null Password", async () => {
  page = await browser.newPage();

  await page.goto("http://rectrack.jltechsol.com/login");

  await page.setViewport({ width: 1366, height: 768 });

  //Click Username
  await page.click(".elevation-12 #email");
  await page.type(".elevation-12 #email", "admin.jammel@gmail.com");
  //Click Password
  await page.waitForSelector(".elevation-12 #password");
  await page.click(".elevation-12 #password");
  //Click Login Button
  await page.waitForSelector(
    ".card-body > .mt-5 > #loginButton > small > span"
  );
  await page.click(".card-body > .mt-5 > #loginButton > small > span");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".elevation-12 #loginButton")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toEqual("LOGIN");
  browser.close();
}, 200000);
