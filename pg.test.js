const randomstring = require("randomstring");
const puppeteer = require("puppeteer");
let page;
let browser;
//Variables
// random first name
const fname = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Middle Name
const mname = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Last Name
const lname = randomstring.generate({
  length: 5,
  charset: "alphabetic"
});
//random Email
const email = randomstring.generate({
  length: 4,
  charset: "alphabetic"
});
//random Mobile number
const mobile = randomstring.generate({
  length: 11,
  charset: "numeric"
});
//========================= L O G I N  A S  H I R I N G  O F F I C E R (PG)   =============================
test("Login as HIRING OFFICER (PG)", async () => {
  browser = await puppeteer.launch({
    headless: true,
    slowMo: 30,
    args: ["--proxy-server= 172.16.1.6:3128"]
  });
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/login");
  await page.setViewport({ width: 1366, height: 768 });
  //Input Username
  await page.click(".elevation-12 #email");
  await page.type(".elevation-12 #email", "pg.jammel@gmail.com");

  //Input Password
  await page.click(".elevation-12 #password");
  await page.type(".elevation-12 #password", "123456789");

  //Login Button
  await page.waitForSelector(".elevation-12 #loginButton");
  await page.click(".elevation-12 #loginButton");

  //dashboard
  await page.waitForSelector(
    ".v-responsive__content > .v-list > .v-list-item > #sidebar1 > .v-list__tile__title"
  );
  await page.click(
    ".v-responsive__content > .v-list > .v-list-item > #sidebar1 > .v-list__tile__title"
  );
  //add applicant
  await page.waitForSelector(".v-responsive #sidebar9");
  await page.click(".v-responsive #sidebar9");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-responsive #sidebar9").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 2000000);

// //==============================  A D D   A P P L I C A N T ======================================
// test("Add applicant", async () => {
//   let input__slotapos;
//   let input__slotadep;

//   // DepartmentID
//   input__slotadep = await page.$("#deptId");
//   await input__slotadep.click({ clickCount: 1 });
//   await input__slotadep.type("MIS");
//   await page.keyboard.press("Enter");

//   // POSITION COMBOBOX
//   input__slotapos = await page.$("#positionId");
//   await input__slotapos.click({ clickCount: 1 });
//   await input__slotapos.type("SQA");
//   await page.keyboard.press("Enter");

//   //auto email
//   await page.click(".v-input #email");
//   await page.type(".v-input #email", `${email}@getnada.com`);

//   //first name
//   await page.click(".v-input #fname");
//   await page.type(".v-input #fname", `${fname}`);
//   //middle name

//   await page.click(".v-input #mname");
//   await page.type(".v-input #mname", `${mname}`);

//   //lastname
//   await page.click(".v-input #lname");
//   await page.type(".v-input #lname", `${lname}`);

//   //Religion
//   await page.click(".v-input #religion");
//   await page.type(".v-input #religion", "Roman Catholic");

//   //Mobile Number
//   await page.click(".v-input #mobile");
//   await page.type(".v-input #mobile", `${mobile}`);

//   //Livestock
//   await page.click(".v-input #livestock");
//   await page.type(".v-input #livestock", "N/A");

//   //Are you willing to Travel
//   input__slotapos = await page.$("#travel");
//   await input__slotapos.click({ clickCount: 1 });
//   await input__slotapos.type("YES");
//   await page.keyboard.press("Enter");

//   await page.click(".v-input #resume_url");
//   await page.type(".v-input #resume_url", "test");

//   //SAVE
//   await page.waitForSelector(".v-card #addapplicant");
//   await page.click(".v-card #addapplicant");

//   const data = await page.evaluate(() => {
//     let Invalid = document.querySelector(".v-card #addapplicant").innerText;
//     return Invalid;
//   });
//   console.log(data);
//   expect(data).toBeDefined();
//   await page.waitFor(2000);
//   await browser.close();
// }, 2000000);

//================================= R E C E I V E D ======================================
//----------------------VIEW ALL LIST OF RECEIVED APPLICANT
test("Validation for Hiring officer can view list of all Received", async () => {
  //DASHBOARD
  await page.waitForSelector(".v-responsive #sidebar1");
  await page.click(".v-responsive #sidebar1");

  //Received
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #received > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #received > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #received > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------Validation Hiring officer can search list of all applicants in Received---------
test("Validation Hiring officer can search list of all applicants in Received", async () => {
  //Search
  await page.waitForSelector(".my-3 > #dashboard1_search #searchinput");
  await page.click(".my-3 > #dashboard1_search #searchinput");

  input__slotsrec = await page.$("#searchinput");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("testupload");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".my-3 > #dashboard1_search #searchinput"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------------Validation for Hiring Officer wants to view Applicants Resume on received applicant-------

test("Validation for Hiring Officer wants to view Applicants Resume on received applicant", async () => {
  await page.waitFor(2000);
  //View Resume
  await page.waitForSelector(
    "tbody > tr:nth-child(1) > td > #receive_view > .v-btn__content"
  );
  await page.click(
    "tbody > tr:nth-child(1) > td > #receive_view > .v-btn__content"
  );

  await page.waitFor(2000);
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr:nth-child(1) > td > #receive_view > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//==========================  O N L I N E  E  X A M  ======================================

//-----------Validation for Hiring officer can view list of all Online Exam-------------
test("Validation for Hiring officer can view list of all Online Exam", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //Online Exam
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #online_exam > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #online_exam > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #online_exam > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------Validation Hiring officer can search list of all applicants in Online Exam---------
test("Validation Hiring officer can search list of all applicants in Online Exam", async () => {
  //Search

  await page.waitForSelector(".my-1 > #dashboard1_search #searchinput");
  await page.click(".my-1 > #dashboard1_search #searchinput");

  input__slotsrec = await page.$("#searchinput");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("BErr");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".my-1 > #dashboard1_search #searchinput"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------------Validation for Hiring Officer wants to view Applicants Resume on Online Exam-------
test("Validation for Hiring Officer wants to view Applicants Resume on Online Exam", async () => {
  //View Resume
  await page.waitFor(2000);
  await page.waitForSelector("tbody > tr > td > #oe_view > .v-btn__content");
  await page.click("tbody > tr > td > #oe_view > .v-btn__content");

  await page.waitFor(2000);
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #oe_view > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//============================= A S S E S S M E N T ==========================================
//-----------Validation for Hiring officer can view list of all Assessment-------------
test("Validation for Hiring officer can view list of all Assessment", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //Assessment
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #assessment > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #assessment > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #assessment > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-----------Validation for Hiring Officer can Search list of all applicant in Assessment-----------
test("Validation for Hiring Officer can Search list of all applicant in Assessment", async () => {
  //Search
  await page.waitForSelector(".my-1 > #dashboard1_search #searchinput");
  await page.click(".my-1 > #dashboard1_search #searchinput");

  input__slotsrec = await page.$("#searchinput");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("qwar");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".my-1 > #dashboard1_search #searchinput"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------Validation for Hiring Officer wants to view Applicants Resume on Assessment---------
test("Validation for Hiring Officer wants to view Applicants Resume on Assessment", async () => {
  //View Resume
  await page.waitFor(2000);
  await page.waitForSelector(".v-datatable tr:nth-child(1) #aview_");
  await page.click(".v-datatable tr:nth-child(1) #aview_");

  await page.waitFor(2000);
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-datatable tr:nth-child(1) #aview_")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-----------Validation for Hiring Officer (PG) can view application form-------------
test("Validation for Hiring Officer can view application form", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //Assessment
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #assessment > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #assessment > .v-tabs__item"
  );

  //Search
  await page.waitForSelector(".my-1 > #dashboard1_search #searchinput");
  await page.click(".my-1 > #dashboard1_search #searchinput");

  input__slotsrec = await page.$("#searchinput");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("qwar");
  await page.keyboard.press("Enter");

  //View application Form
  await page.waitForSelector(".v-datatable #endorse_exam");
  await page.click(".v-datatable #endorse_exam");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-datatable #endorse_exam")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//==================================  S H O R T L I S T E D ==================================

//-----------Validation for Hiring officer can view list of all Shortlisted-------------
test("Validation for Hiring officer can view list of all Shortlisted", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //Shortlisted
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #ShortlistedUsers > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #ShortlistedUsers > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #ShortlistedUsers > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-----------Validation for Hiring Officer can Search list of all applicant in Shortlisted-----------
test("Validation for Hiring Officer can Search list of all applicant in Shortlisted", async () => {
  //Search
  await page.waitForSelector(".my-1 > #dashboard1_search #searchinput");
  await page.click(".my-1 > #dashboard1_search #searchinput");

  input__slotsrec = await page.$("#searchinput");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("AKsQ");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".my-1 > #dashboard1_search #searchinput"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------Validation for Hiring Officer wants to view Applicants Resume on Shortlisted---------
test("Validation for Hiring Officer wants to view Applicants Resume on Shortlisted", async () => {
  //View Resume
  await page.waitForSelector(
    "tbody > tr > td > #shortlisted_view > .v-btn__content"
  );
  await page.click("tbody > tr > td > #shortlisted_view > .v-btn__content");

  await page.waitFor(2000);
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #shortlisted_view > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//============================ P E N D I N G    F O R    R E V I E W  ============================
//-----------Validation for Hiring officer can view list of all Pending for Review-------------
test("Validation for Hiring officer can view list of all Pending for Review", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //Shortlisted
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #pending_for_review > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #pending_for_review > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #pending_for_review > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-----------Validation for Hiring Officer can Search list of all applicant in pending for review-----------
test("Validation for Hiring Officer can Search list of all applicant in Pending for Review", async () => {
  //Search
  await page.waitForSelector(".my-1 > #dashboard1_search #searchinput");
  await page.click(".my-1 > #dashboard1_search #searchinput");

  input__slotsrec = await page.$("#searchinput");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("AaIU");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".my-1 > #dashboard1_search #searchinput"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------Validation for Hiring Officer wants to view Applicants Resume on pending for review---------
test("Validation for Hiring Officer wants to view Applicants Resume on Pending for Review", async () => {
  //View Resume
  await page.waitForSelector("tbody > tr > td > #pview_ > .v-btn__content");
  await page.click("tbody > tr > td > #pview_ > .v-btn__content");

  await page.waitFor(2000);
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #pview_ > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//====================================  D E P L O Y E D   ==========================================
//---------Validation for Hiring Officer (PG) can view list of all applicants in Deployed--------------
test("Validation for Hiring Officer (PG) can view list of all applicants in Deployed", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //Deployed
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #deployed > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #deployed > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #deployed > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-----------Validation for Hiring Officer (PG) can search for applicant in Deployed-----------
test("Validation for Hiring Officer (PG) can search for applicant in Deployed", async () => {
  //Search
  await page.waitForSelector(".my-1 > #dashboard1_search #searchinput");
  await page.click(".my-1 > #dashboard1_search #searchinput");

  input__slotsrec = await page.$("#searchinput");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("bhVe");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".my-1 > #dashboard1_search #searchinput"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------Validation for Hiring Officer (PG) can view resume of applicant in Deployed---------
test("Validation for Hiring Officer wants to view Applicants Resume on Assessment", async () => {
  //View Resume
  await page.waitForSelector(
    "tbody > tr > td > #shortlisted_view > .v-btn__content"
  );
  await page.click("tbody > tr > td > #shortlisted_view > .v-btn__content");

  await page.waitFor(2000);
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #shortlisted_view > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//-----------Validation for Hiring Officer (PG) can change deployed date of applicant-----------
test("Validation for Hiring Officer (PG) can change deployed date of applicant", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //Deployed
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #deployed > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #deployed > .v-tabs__item"
  );

  //SEARCH
  await page.waitForSelector(".my-1 > #dashboard1_search #searchinput");
  await page.click(".my-1 > #dashboard1_search #searchinput");

  input__slotsrec = await page.$("#searchinput");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("bhVe");
  await page.keyboard.press("Enter");

  //click change deployment date
  await page.waitForSelector(".v-datatable #change_sched");
  await page.click(".v-datatable #change_sched");

  await page.waitForSelector(
    ".v-input > .v-input__control > .v-input__slot > .v-text-field__slot > input"
  );
  await page.click(
    ".v-input > .v-input__control > .v-input__slot > .v-text-field__slot > input"
  );

  await page.waitForSelector(
    "tbody > tr > td > .v-btn--outline > .v-btn__content"
  );
  await page.click("tbody > tr > td > .v-btn--outline > .v-btn__content");

  await page.waitForSelector(
    ".v-dialog > .v-picker > .v-picker__actions > #dateSave > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-picker > .v-picker__actions > #dateSave > .v-btn__content"
  );

  //save
  await page.waitForSelector(
    ".v-dialog > .v-card > .v-card__actions > #setSchedSave > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-card > .v-card__actions > #setSchedSave > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > #setSchedSave > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);
//=====================================  E N D O R S E M E N T =======================================

//=====================================  A S S E S S M E N T  =======================================
//-------Validation for Hiring Officer (PG) can view list of all applications' in Assestment--------
test("Validation for Hiring Officer (PG) can view list of all applications' in Assestment", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //ENDORSEMENT (SIDEBAR)
  await page.waitForSelector(".v-responsive #sidebar2");
  await page.click(".v-responsive #sidebar2");

  //ASSESSMENT
  await page.waitForSelector(
    ".v-tabs__wrapper > .v-tabs__container > #assessment > .v-tabs__item > .v-badge"
  );
  await page.click(
    ".v-tabs__wrapper > .v-tabs__container > #assessment > .v-tabs__item > .v-badge"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__wrapper > .v-tabs__container > #assessment > .v-tabs__item > .v-badge"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-----------------Validation for Hiring Officer (PG) can search applicant------------------
test("Validation for Hiring Officer (PG) can search applicant", async () => {
  //Search
  await page.waitForSelector("input#endorsement_search");
  await page.click("input#endorsement_search");

  input__slotsrec = await page.$("#endorsement_search");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("vijonad");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#endorsement_search").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//--------------------Validation for Hiring Officer (PG) can view resume of applicant---------------
test("Validation for Hiring Officer wants to view Applicants Resume on Assessment", async () => {
  //View Resume
  await page.waitForSelector(".v-datatable #assessment_view");
  await page.click(".v-datatable #assessment_view");

  await page.waitFor(2000);
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-datatable #assessment_view")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//----------------------Validation for Hiring Office (PG) can reject applicant------------
test("Validation for Hiring Office (PG) can reject applicant", async () => {
  let input__slotsapp;
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //ENDORSEMENT (SIDEBAR)
  await page.waitForSelector(".v-responsive #sidebar2");
  await page.click(".v-responsive #sidebar2");

  //ASSESSMENT
  await page.waitForSelector(
    ".v-tabs__wrapper > .v-tabs__container > #assessment > .v-tabs__item > .v-badge"
  );
  await page.click(
    ".v-tabs__wrapper > .v-tabs__container > #assessment > .v-tabs__item > .v-badge"
  );

  //Search
  await page.waitForSelector("input#endorsement_search");
  await page.click("input#endorsement_search");

  input__slotsrec = await page.$("#endorsement_search");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("vijonad");
  await page.keyboard.press("Enter");

  //reject button
  await page.waitForSelector(".v-datatable #reject");
  await page.click(".v-datatable #reject");

  //Click Remarks
  await page.click(".v-input #remarks");
  await page.type(".v-input #remarks", "not good");

  //Save Button
  await page.waitForSelector(".v-dialog__content #setReject");
  await page.click(".v-dialog__content #setReject");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-dialog__content #setReject")
      .innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);

//====================================  S H O R T L I S T E D   ==========================================
//----------Validation for Hiring Officer (PG) can view list of all applications' in Shortlisted-----------
test("Validation for Hiring Officer (PG) can view list of all applications' in Shortlisted", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //ENDORSEMENT (SIDEBAR)
  await page.waitForSelector(".v-responsive #sidebar2");
  await page.click(".v-responsive #sidebar2");

  //SHORTLISTED
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #shortlisted > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #shortlisted > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #shortlisted > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-----------------Validation for Hiring Officer (PG) can search applicant------------------
test("Validation for Hiring Officer (PG) can search applicant", async () => {
  //Search
  await page.waitForSelector("input#endorsement_search");
  await page.click("input#endorsement_search");

  input__slotsrec = await page.$("#endorsement_search");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("AKsQ");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#endorsement_search").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//--------------------Validation for Hiring Officer (PG) can view resume of applicant---------------
test("Validation for Hiring Officer wants to view Applicants Resume on Assessment", async () => {
  //View Resume
  await page.waitForSelector(
    "tbody > tr > td > #shortlisted_view > .v-btn__content"
  );
  await page.click("tbody > tr > td > #shortlisted_view > .v-btn__content");

  await page.waitFor(2000);
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #shortlisted_view > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitFor(2000);
}, 200000);
//--------------Validation for Hiring Officer (PG) can subject applicant for job offer-------------
test("Validation for Hiring Officer (PG) can subject applicant for job offer", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //ENDORSEMENT (SIDEBAR)
  await page.waitForSelector(".v-responsive #sidebar2");
  await page.click(".v-responsive #sidebar2");

  //SHORTLISTED
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #shortlisted > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #shortlisted > .v-tabs__item"
  );
  //Search
  await page.waitForSelector("input#endorsement_search");
  await page.click("input#endorsement_search");

  input__slotsrec = await page.$("#endorsement_search");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("AKsQ");
  await page.keyboard.press("Enter");

  //JOB OFFER
  await page.waitForSelector(".v-datatable #joborder");
  await page.click(".v-datatable #joborder");

  //Show dialog box
  page.on("dialog", async dialog => {
    await dialog.accept();
  });
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-datatable #joborder").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//=====================================   J O B  O F F E R   =====================================
//---------Validation for Hiring Officer (PG) can view list of all applications' in Job Offer--------
test("Validation for Hiring Officer (PG) can view list of all applications' in Job Offer", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //ENDORSEMENT (SIDEBAR)
  await page.waitForSelector(".v-responsive #sidebar2");
  await page.click(".v-responsive #sidebar2");

  //JOB OFFER
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #job_offer > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #job_offer > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #job_offer > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-----------------Validation for Hiring Officer (PG) can search applicant------------------
test("Validation for Hiring Officer (PG) can search applicant", async () => {
  //Search
  await page.waitForSelector("input#endorsement_search");
  await page.click("input#endorsement_search");

  input__slotsrec = await page.$("#endorsement_search");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("jiwoxyh");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#endorsement_search").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//--------------------Validation for Hiring Officer (PG) can view resume of applicant---------------
test("Validation for Hiring Officer wants to view Applicants Resume on Assessment", async () => {
  //View Resume
  await page.waitForSelector(
    "tbody > tr > td > #joboffer_view > .v-btn__content"
  );
  await page.click("tbody > tr > td > #joboffer_view > .v-btn__content");

  await page.waitFor(2000);
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #joboffer_view > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-----------------Validation for Hiring Office (PG) can deploy applicant on what day------------------
test("Validation for Hiring Office (PG) can deploy applicant on what day", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //ENDORSEMENT (SIDEBAR)
  await page.waitForSelector(".v-responsive #sidebar2");
  await page.click(".v-responsive #sidebar2");

  //JOB OFFER
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #job_offer > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #job_offer > .v-tabs__item"
  );

  //Search
  await page.waitForSelector("input#endorsement_search");
  await page.click("input#endorsement_search");

  input__slotsrec = await page.$("#endorsement_search");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("jiwoxyh");
  await page.keyboard.press("Enter");

  //deploy button
  await page.waitForSelector("button#deploy");
  await page.click("button#deploy");

  //click date
  await page.waitForSelector(
    ".v-input--is-focused > .v-input__control > .v-input__slot > .v-text-field__slot > input"
  );
  await page.click(
    ".v-input--is-focused > .v-input__control > .v-input__slot > .v-text-field__slot > input"
  );

  await page.waitForSelector(
    "tbody > tr > td > .v-btn--outline > .v-btn__content"
  );
  await page.click("tbody > tr > td > .v-btn--outline > .v-btn__content");

  await page.waitForSelector(
    ".v-dialog > .v-picker > .v-picker__actions > #dateSave > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-picker > .v-picker__actions > #dateSave > .v-btn__content"
  );

  //save deploy date
  await page.waitForSelector(
    ".v-dialog--active > .v-card > .v-card__actions > #setSchedSave > .v-btn__content"
  );
  await page.click(
    ".v-dialog--active > .v-card > .v-card__actions > #setSchedSave > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog--active > .v-card > .v-card__actions > #setSchedSave > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-------------------Validation for Hiring Office (PG) can kept for reference the applicant----------
test("Validation for Hiring Office (PG) can kept for reference the applicant", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //ENDORSEMENT (SIDEBAR)
  await page.waitForSelector(".v-responsive #sidebar2");
  await page.click(".v-responsive #sidebar2");

  //JOB OFFER
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #job_offer > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #job_offer > .v-tabs__item"
  );

  //Search
  await page.waitForSelector("input#endorsement_search");
  await page.click("input#endorsement_search");

  input__slotsrec = await page.$("#endorsement_search");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("sofyxif");
  await page.keyboard.press("Enter");

  //kept for reference button
  await page.waitForSelector(
    "tbody > tr > td > #kept_for_ref > .v-btn__content"
  );
  await page.click("tbody > tr > td > #kept_for_ref > .v-btn__content");

  //Show dialog box
  page.on("dialog", async dialog => {
    await dialog.accept();
  });
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > td > #kept_for_ref > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//----------------------Validation for Hiring Officer (PG) can add Referral----------------------
test("Validation for Hiring Officer (PG) can add Referral", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //ENDORSEMENT (SIDEBAR)
  await page.waitForSelector(".v-responsive #sidebar2");
  await page.click(".v-responsive #sidebar2");

  //JOB OFFER
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #job_offer > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #job_offer > .v-tabs__item"
  );

  //Search
  await page.waitForSelector("input#endorsement_search");
  await page.click("input#endorsement_search");

  input__slotsrec = await page.$("#endorsement_search");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("kiel");
  await page.keyboard.press("Enter");

  //add referrer button
  await page.waitForSelector(
    "tbody > tr:nth-child(1) > td > #addRefForm > .v-btn__content"
  );
  await page.click(
    "tbody > tr:nth-child(1) > td > #addRefForm > .v-btn__content"
  );

  //referrer id
  await page.waitForSelector(
    "#addModal___BV_modal_body_ > #form > #__BVID__376 #id-input"
  );
  await page.click(
    "#addModal___BV_modal_body_ > #form > #__BVID__376 #id-input"
  );

  //referral dept
  await page.waitForSelector(
    "#addModal___BV_modal_body_ > #form > #__BVID__380 #dept-input"
  );
  await page.click(
    "#addModal___BV_modal_body_ > #form > #__BVID__380 #dept-input"
  );

  //referer position
  await page.waitForSelector(
    "#addModal___BV_modal_body_ > #form > #__BVID__382 #pos-input"
  );
  await page.click(
    "#addModal___BV_modal_body_ > #form > #__BVID__382 #pos-input"
  );

  //referrer contact
  await page.waitForSelector(
    "#addModal___BV_modal_body_ > #form > #__BVID__384 #contact-input"
  );
  await page.click(
    "#addModal___BV_modal_body_ > #form > #__BVID__384 #contact-input"
  );

  //referrer relationship to referral
  await page.waitForSelector(
    "#addModal___BV_modal_body_ > #form > #__BVID__392 #rel-input"
  );
  await page.click(
    "#addModal___BV_modal_body_ > #form > #__BVID__392 #rel-input"
  );

  //Done button
  await page.waitForSelector(
    ".modal-dialog > #addModal___BV_modal_content_ > #addModal___BV_modal_body_ #submitModal"
  );
  await page.click(
    ".modal-dialog > #addModal___BV_modal_content_ > #addModal___BV_modal_body_ #submitModal"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".modal-dialog > #addModal___BV_modal_content_ > #addModal___BV_modal_body_ #submitModal"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//====================================  R E P O R T S ================================================
//--------Validation for Hiring Officer wants to view reports of Kept for Reference of applicants--------
test("Validation for User wants to view reports of Kept for Reference of applicants", async () => {
  page = await browser.newPage();
  await page.goto("http://rectrack.jltechsol.com/dashboard1");
  await page.setViewport({ width: 1366, height: 768 });

  //Reports
  await page.waitForSelector(
    ".v-responsive__content > .v-list > .v-list-item > #sidebar6 > .v-list__tile__title"
  );
  await page.click(
    ".v-responsive__content > .v-list > .v-list-item > #sidebar6 > .v-list__tile__title"
  );
  //kept for Reference
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #keptforRef > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #keptforRef > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #keptforRef > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-----------Validation for Hiring Officer (PG) can search for applicant in Kept for Reference-----------
test("Validation for Hiring Officer (PG) can search for applicant in Kept for Reference", async () => {
  //Search
  await page.waitForSelector("input#searhReports");
  await page.click("input#searhReports");

  input__slotsrec = await page.$("#searhReports");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("civodohe");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#searhReports").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  await page.waitForSelector("button#search");
  await page.click("button#search");
}, 200000);

//------------------Validation for Hiring Officer wants to view reports in Deployed of applicants--------------
test("Validation for Hiring Officer wants to view reports in Deployed of applicants", async () => {
  //Deployed
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #deplyed > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #deplyed > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #deplyed > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-----------Validation for Hiring Officer (PG) can search for applicant in Deployed-----------
test("Validation for Hiring Officer (PG) can search for applicant in Deployed", async () => {
  //Search
  await page.waitForSelector("input#searhReports");
  await page.click("input#searhReports");

  input__slotsrec = await page.$("#searhReports");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("aEHf");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#searhReports").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  //clear search
  await page.waitForSelector("button#search");
  await page.click("button#search");
}, 200000);

//------------------Validation for User wants to view reports in Blacklisted of applicants--------------
test("Validation for User wants to view reports in Blacklisted of applicants", async () => {
  //blacklisted
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #blacklisted > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #blacklisted > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #blacklisted > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);
//-----------Validation for Hiring Officer (PG) can search for applicant in Blacklisted-----------
test("Validation for Hiring Officer (PG) can search for applicant in Blacklisted", async () => {
  //Search
  await page.waitForSelector("input#searhReports");
  await page.click("input#searhReports");

  input__slotsrec = await page.$("#searhReports");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("IOLu");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#searhReports").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  //clear search
  await page.waitForSelector("button#search");
  await page.click("button#search");
}, 200000);

//-------------Validation for Hiring Officer (PG) wants to view reports in Rejected of applicants-----------
test("Validation for User wants to view reports in Rejected of applicants", async () => {
  //Rejected
  await page.waitForSelector(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #rected > .v-tabs__item"
  );
  await page.click(
    ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #rected > .v-tabs__item"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-tabs__bar > .v-tabs__wrapper > .v-tabs__container > #rected > .v-tabs__item"
    ).innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
}, 200000);

//-----------Validation for Hiring Officer (PG) can search for applicant in Rejected-----------
test("Validation for Hiring Officer (PG) can search for applicant in Rejected", async () => {
  //Search
  await page.waitForSelector("input#searhReports");
  await page.click("input#searhReports");

  input__slotsrec = await page.$("#searhReports");
  await input__slotsrec.click({ clickCount: 1 });
  await input__slotsrec.type("FMpG");
  await page.keyboard.press("Enter");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector("input#searhReports").innerText;
    return Invalid;
  });
  console.log(data);
  expect(data).toBeDefined();
  //clear search
  await page.waitForSelector("button#search");
  await page.click("button#search");
  await browser.close();
}, 200000);
